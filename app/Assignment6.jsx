"use strict";
//https://pokeapi.co/docs/v2
const { useState, useEffect } = React;
//component for left hand view

function ListOfPokemon({ pokemons, onPokemonClick }) {
  return (
    <ul>
      {pokemons.map((pokemon) => (
        <li
          id={pokemon.pokemon.name}
          key={pokemon.pokemon.name}
          onClick={() => onPokemonClick(pokemon.pokemon.name)}
        >
          {pokemon.pokemon.name}
        </li>
      ))}
    </ul>
  );
}

function ShowPokemonInformation({ selectedPokemon }) {
  if (!selectedPokemon) {
    return (
      <p id="nopokemonselected">
        Please look up a Pokemon from a certain type!
      </p>
    );
  }
  return (
    <div>
      <u>
        <h2 id="nameHeader">
          {selectedPokemon.name.charAt(0).toUpperCase() +
            selectedPokemon.name.slice(1)}
        </h2>
      </u>
      <div id="headercontainer">
        <h2 className="pokemonheader">Normal </h2>
        <h2 className="pokemonheader">VS</h2>
        <h2 className="pokemonheader">Shiny </h2>
      </div>

      <div id="pokemonimagescontainer">
        <img
          className="pokemonimages"
          src={selectedPokemon.image}
          alt={selectedPokemon.name}
        />
        <img
          className="pokemonimages"
          src={selectedPokemon.shinyimage}
          alt={selectedPokemon.name}
        />
      </div>
      <div id="moves-stats-container">
        <div className="moves-container">
          <u>
            <h3>
              Moves{" "}
              {selectedPokemon.name.charAt(0).toUpperCase() +
                selectedPokemon.name.slice(1)}{" "}
              learns:
            </h3>
          </u>
          <ul>
            {selectedPokemon.moves.map((move, index) => (
              <li key={index}>
                {move.move.name.charAt(0).toUpperCase() +
                  move.move.name.slice(1)}
              </li>
            ))}
          </ul>
        </div>
        <div className="stats-container">
          <u>
            <h3>
              {" "}
              {selectedPokemon.name.charAt(0).toUpperCase() +
                selectedPokemon.name.slice(1)}
              's base stats
            </h3>
          </u>
          <ul>
            {selectedPokemon.stats.map((stat, index) => (
              <li key={index}>
                {stat.stat.name.charAt(0).toUpperCase() +
                  stat.stat.name.slice(1)}
                : {stat.base_stat}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

function App() {
  const [types, setTypes] = useState([]);
  const [pokemons, setPokemons] = useState([]);
  const [selectedPokemon, setSelectedPokemon] = useState(null);
  function getPokemons() {
    const typeOfPokemons = document.querySelector("select");
    fetch(`https://pokeapi.co/api/v2/type/${typeOfPokemons.value}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Not 2xx response", { cause: response });
        }
        return response.json();
      })
      .then((obj) => {
        setPokemons(obj.pokemon);
      })
      .catch((err) => {
        console.error("3)Error:", err);
      });
  }
  function onPokemonClick(pokemonName) {
    fetch(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
      .then((response) => response.json())
      .then((pokemon) => {
        setSelectedPokemon({
          name: pokemon.name,
          image: pokemon.sprites.other["official-artwork"].front_default,
          shinyimage: pokemon.sprites.other["official-artwork"].front_shiny,
          moves: pokemon.moves,
          stats: pokemon.stats,
        });
      })
      .catch((err) => {
        console.error("Error fetching Pokemon details:", err);
      });
  }
  useEffect(() => {
    fetch("https://pokeapi.co/api/v2/type")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Not 2xx response", { cause: response });
        }
        return response.json();
      })
      .then((obj) => {
        const filteredTypes = obj.results.filter(
          (type) => type.name !== "shadow" && type.name !== "unknown"
        );
        setTypes(filteredTypes);
      })
      .catch((err) => {
        console.error("3)Error:", err);
      });
  }, []);

  return (
    <div className="wrapper">
      <header>
        <h1>Pokemon Searcher</h1>
        <div id="selectwrapper">
          <p>Select A Type:</p>
          <select onChange={getPokemons}>
            {types.map((type) => (
              <option>{type.name}</option>
            ))}
          </select>
        </div>
        <ListOfPokemon
          pokemons={pokemons}
          onPokemonClick={onPokemonClick}
        ></ListOfPokemon>
      </header>
      <main>
        <ShowPokemonInformation selectedPokemon={selectedPokemon} />
      </main>
    </div>
  );
}
ReactDOM.render(<App />, document.querySelector("#react-root"));
