#ASSIGNMENT 5 Docker image dockerfile
FROM httpd

#User Information DOCKERHUB
LABEL username="rebolto" email="noah.diplarakis@dawsoncollege.qc.ca"

#DocumentRoot setting workdir to the doc root of httpd
WORKDIR /usr/local/apache2/htdocs/

#Copying the app to the doc root

COPY app/ /usr/local/apache2/htdocs/
